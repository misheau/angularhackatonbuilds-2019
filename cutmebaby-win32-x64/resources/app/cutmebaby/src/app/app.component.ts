import { Component } from '@angular/core';
declare var electron: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'cutmebaby';
  status: string = 'main'
  // fileToUpload: File = null;
  // starttime: string
  // endtime: string
  // filepath: string

  // handleFileInput(files: FileList) {
  //   this.fileToUpload = files.item(0);
  //   var reader  = new FileReader();

  // }
  setLoader(){
    this.status = 'working'
  }

  back(){
    this.status = 'main'
  }

  submitCut(filename,starttime,endtime){
    setTimeout(()=>{
    this.status = electron.ipcRenderer.sendSync('cut-video',{starttime:starttime,endtime:endtime,filename:filename}); 
    },1000);
  }

}
