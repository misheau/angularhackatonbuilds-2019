var electron = require("electron");
var app = electron.app; // Module to control application life.
var BrowserWindow = electron.BrowserWindow; // Module to create native browser window.
var os = require("os");
var Menu = electron.Menu;
var path = require("path");
var ffmpeg = require('fluent-ffmpeg');
/**
 *    input - string, path of input file
 *    output - string, path of output file
 *    callback - function, node-style callback fn (error, result)        
 */

 electron.ipcMain.on('cut-video',(event,arg)=>{
    cutVideo(arg.filename, 'output.webm', arg.starttime, arg.endtime, function(err){
      if(!err) {
          event.returnValue = 'complete'
          //...    
      }else{
          event.returnValue = 'error' 
       //   'An error occured - '+err+': '+arg.starttime+' - '+arg.endtime
      }
    });
  //  console.log('cut me baby')
 })

function cutVideo(input, output,starttime, endtime, callback) {
    ffmpeg(input)
        .output(output)
				.outputOptions([
				'-ss '+starttime,
				'-t '+endtime
				])
        .on('end', function() {                    
            console.log('conversion ended');
            callback(null);
        }).on('error', function(err){
           // console.log('error: ', e.code, e.msg);
						console.log('an error occured'+err)
            callback(err);
        }).run();
}

//
app.on("ready", function() {
  const { width, height } = electron.screen.getPrimaryDisplay().workAreaSize;
  var mainWindow = new BrowserWindow({
    //win = new BrowserWindow({width, height})
    //fullscreen: true
    width: width,
    height: height,
    icon: path.join(__dirname, 'src/assets/logo.png')
  });
	mainWindow.loadURL("file:///" + __dirname + "/cutmebaby/dist/cutmebaby/index.html");
  	mainWindow.on("closed", function() {
    	mainWindow = null;
	});
});
